using pl_api.Models;
using pl_api.ViewModel;

public interface IRecipientService
{
    public Recipient Add(RecipientView recipient);
    public Application SignDocument(RecipientView app);
    dynamic GetListByID(string id);
}
using pl_api.Models;
using pl_api.ViewModel;

public interface IUsersService
{
    public string Add(UserView person);
    public User Update(UserView person);

     public dynamic Login(UserView value);
     public User Get(string id);
}
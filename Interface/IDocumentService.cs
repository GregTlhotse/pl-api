using pl_api.Models;
using pl_api.ViewModel;

public interface IDocumentService
{
    public Document Add(DocumentView document);
    List<Document> GetList();
    
}
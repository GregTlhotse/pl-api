

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pl_api.Models;
using pl_api.ViewModel;
using WebApi.Helpers;

public class DocumentService:IDocumentService
{
    readonly DataContext _dbcontext;
    public DocumentService(DataContext dbcontext)
    {
        _dbcontext = dbcontext;

    }
    public Document Add(DocumentView document)
    {
        try
        {
            var doc = new Document
            {
                RecipientId = document.RecipientId,
                Name = document.Name,
                IsActive = true
            };
             _dbcontext.Add(doc);
            _dbcontext.SaveChanges();
            
            return doc;

        }
        catch (Exception ex)
        {
            

        }
        return null;

    }
    public List<Document> GetList(){
        return _dbcontext.Documents.Where(x => x.IsActive == true).ToList();

    }
}
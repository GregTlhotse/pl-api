
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

public class MergeFilesService:IMergeFilesService{
    
    public  void MergePDFs(){
        DirectoryInfo di = new DirectoryInfo("UploadedFiles");
                    FileInfo[] files = di.GetFiles("*.pdf");

                        System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

     
        using (PdfDocument one = PdfReader.Open(files[0].FullName, PdfDocumentOpenMode.Import))
        using (PdfDocument two = PdfReader.Open(files[1].FullName, PdfDocumentOpenMode.Import))
        using (PdfDocument three = PdfReader.Open(files[2].FullName, PdfDocumentOpenMode.Import))
        using (PdfDocument outPdf = new PdfDocument())
        {                
            CopyPages(one, outPdf);
            CopyPages(two, outPdf);
            CopyPages(three, outPdf);

            outPdf.Save("Merged_Platimum_Life.pdf");
        }

        
    }
    void CopyPages(PdfDocument from, PdfDocument to)
        {
            for (int i = 0; i < from.PageCount; i++)
            {
                to.AddPage(from.Pages[i]);
            }
        }
    
}
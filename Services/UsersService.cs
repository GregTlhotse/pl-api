

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pl_api.Models;
using pl_api.ViewModel;
using WebApi.Helpers;

public class UsersService:IUsersService
{
    readonly DataContext _dbcontext;
    public UsersService(DataContext dbcontext)
    {
        _dbcontext = dbcontext;

    }
	public User Get(string id){
		return _dbcontext.Users.Where(x => x.Id == Guid.Parse(id)).FirstOrDefault();
	}
    public string Add(UserView person) {
        try{
            var user = _dbcontext.Users.Where(x => x.Email == person.Email.Trim()).Count();
            if(user>0){
				if(person.Id != null) {
					var usrCheck = _dbcontext.Users.Where(x => x.Id == Guid.Parse(person.Id)).Count();

				if(usrCheck>0){
					 Update(person);
                    return "Updated User";
				}
				}
                return "Email already exist";
            }

            byte[] hashedPassword = EncryptPassword(person.Password);
            var personInfo = new User{
                Name = person.Name,
                Surname = person.Surname,
                Role = person.Role,
                Email = person.Email,
                Phonenumber = person.Phonenumber,
                Password = hashedPassword,
                CreatedDate = DateTime.Now, 
				CreatedUser = person.Name,
			    IsActive = true
			   

            }; 
            _dbcontext.Add(personInfo);
            _dbcontext.SaveChanges();
            return personInfo.Id.ToString();
            }catch(Exception ex){
                return ex.Message;

            }

    }
    public User Update(UserView person) {
		  var user = _dbcontext.Users.Where(x => x.Id == Guid.Parse(person.Id)).FirstOrDefault();

			user.Name = person.Name;
			user.Surname = person.Surname;
            user.Role = person.Role;
			user.Email = person.Email;
			user.Phonenumber = person.Phonenumber;
			user.ModifiedDate = DateTime.Now;
			user.ModifiedUser = person.Name;
 
		   _dbcontext.Entry(user).State = EntityState.Modified;
		   _dbcontext.SaveChanges();
		   return user;
		}
         byte[] EncryptPassword(string password)
		{
			byte[] data = System.Text.Encoding.ASCII.GetBytes(password);
			MD5 md5 = MD5.Create();
			byte[] hash = md5.ComputeHash(data);
			return hash;
		}
        bool CompareBytes(byte[] databasePassword, byte[] userPassword)
		{
			bool bEqual = false;
			if (userPassword.Length == databasePassword.Length)
			{
				int i = 0;
				while ((i < userPassword.Length) && (userPassword[i] == databasePassword[i]))
				{
					i += 1;
				}
				if (i == userPassword.Length)
				{
					bEqual = true;
				}
			}
			return bEqual;
		}
        public dynamic Login(UserView value){
            try
			{
				byte[] hashedPassword = EncryptPassword(value.Password);
				var user = _dbcontext.Users.Where(x => x.Email == value.Email && x.IsActive == true).FirstOrDefault();
				
				if (user == null)
				{
					return ("User not found");
				}

				bool passwordsMatch = CompareBytes(user.Password, hashedPassword);
				if(passwordsMatch == false) {
					return ("Incorrect Password");
				}
				
				var claims = new[]
				{
							new Claim(ClaimTypes.Name, user.Email),
							new Claim(ClaimTypes.Email, user.Email)
							 };


				var token = new JwtSecurityToken(expires: DateTime.Now.AddMonths(6));
	
                // return "token: "+token.ToString();
				return (new
				{
					user,
					token = new JwtSecurityTokenHandler().WriteToken(token),
				});

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return "Username and Password Not found, Please sign-up first";
			}

        }

}
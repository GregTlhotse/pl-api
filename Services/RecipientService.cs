

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pl_api.Models;
using pl_api.ViewModel;
using WebApi.Helpers;

public class RecipientService:IRecipientService
{
    readonly DataContext _dbcontext;
    readonly IMergeFilesService _mergeFilesService;
    public RecipientService(DataContext dbcontext,IMergeFilesService mergeFilesService)
    {
        _dbcontext = dbcontext;
        _mergeFilesService = mergeFilesService;

    }
    public Recipient Add(RecipientView recipient)
    {
        try
        {
            var recipientDetails = new Recipient
            {
                UserId = recipient.UserId,
                Name = recipient.Name,
                Surname = recipient.Surname,
                IsActive = true
            };
            var data = _dbcontext.Add(recipientDetails);
            _dbcontext.SaveChanges();
            if (data != null)
            {
                var appData = new Application
                {
                    RecipientId = recipientDetails.Id.ToString(),
                    Department = recipient.Department,
                    CompanyName = recipient.CompanyName,
                    PaymentDescription = recipient.PaymentDescription,
                    BankName = recipient.BankName,
                    BankAccount = recipient.BankAccount,
                    BranchCode = recipient.BranchCode,
                    Dopr = recipient.Dopr,
                    Doi = recipient.Doi,
                    IsActive = true,
                    IsSigned = false
                };
                _dbcontext.Add(appData);
                _dbcontext.SaveChanges();
            }
            return recipientDetails;

        }
        catch (Exception ex)
        {
            

        }
        return null;

    }
    public Application SignDocument(RecipientView app){
        var getApplication = _dbcontext.Applications.Where(x => x.RecipientId == app.Id).FirstOrDefault();

        if(getApplication != null){
            getApplication.IsSigned = true;
             _dbcontext.Entry(getApplication).State = EntityState.Modified;
		   _dbcontext.SaveChanges();
            _mergeFilesService.MergePDFs();
        }
        return getApplication;
    }
    public List<Application> GetList(){
        return _dbcontext.Applications.Where(x => x.IsActive == true).ToList();

    }
    public dynamic GetListByID(string id){
        var query = (from app in _dbcontext.Applications
                        join rc in _dbcontext.Recipients on app.RecipientId equals rc.Id.ToString()
                        join user in _dbcontext.Users on rc.UserId equals user.Id.ToString()
                        where user.Id == Guid.Parse(id)
                        select new { Application = app, Recipient = rc });
        return query;

    }
}
using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Threading.Tasks;  
  
namespace pl_api.Models  
{  
    public class User: Base{
        
        public  string Name { get; set; }
         public  string Surname { get; set; }
         public  string Email { get; set; }
         public  string Phonenumber { get; set; }
         public  string Role { get; set; }

         public  byte[] Password { get; set; }
         public string PasswordResetCode { get; set; }
         public DateTime? PasswordResetTime { get; set; }

    }  
}  
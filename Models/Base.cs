using Microsoft.EntityFrameworkCore;
using System;

namespace pl_api.Models{
    public class Base{
        public Guid Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
        public bool IsActive  { get; set; }


    }
}

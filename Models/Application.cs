using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Threading.Tasks;  
  
namespace pl_api.Models  
{  
    public class Application: Base{
        public  string RecipientId { get; set; }
        public  string Department { get; set; }
        public  string CompanyName { get; set; }
         public  string PaymentDescription { get; set; }
        public  string BankName { get; set; }

         public  string BankAccount { get; set; }

         public  string BranchCode { get; set; }
         public DateTime Dopr { get; set; }
         public DateTime Doi { get; set; }
         public bool IsSigned { get; set; }



    }  
}  
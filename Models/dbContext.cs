namespace WebApi.Helpers;

using Microsoft.EntityFrameworkCore;
using pl_api.Models;

public class DataContext : DbContext
{
    protected readonly IConfiguration Configuration;

    public DataContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {

         options.UseSqlServer(Configuration.GetConnectionString("WebApiDatabase"));
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Recipient> Recipients { get; set; }
    public DbSet<Application> Applications { get; set; }
    public DbSet<Document> Documents { get; set; }
}
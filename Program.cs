using WebApi.Helpers;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddTransient<IStreamFileUploadService, StreamFileUploadLocalService>();
builder.Services.AddTransient<IMergeFilesService, MergeFilesService>();
builder.Services.AddTransient<IDocumentService, DocumentService>();
builder.Services.AddTransient<IRecipientService, RecipientService>();
builder.Services.AddTransient<IUsersService, UsersService>();
builder.Services.AddSingleton<DataContext>();




//services cors
    builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
    {
        builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
    }));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseCors("corsapp");
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

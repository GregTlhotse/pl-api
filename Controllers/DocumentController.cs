using Microsoft.AspNetCore.Mvc;
using pl_api.ViewModel;

[Route("api/[controller]")]
[ApiController]
public class DocumentController : ControllerBase
{
    readonly IDocumentService _service;
        
    public DocumentController(IDocumentService service)
    {
        _service = service;
    }
        [HttpPost]
        public IActionResult Post([FromBody] DocumentView app)
        {
             return Ok(_service.Add(app));

        }




}
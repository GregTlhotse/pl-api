using Microsoft.AspNetCore.Mvc;
using pl_api.ViewModel;

[Route("api/[controller]")]
[ApiController]
public class ApplicationController : ControllerBase
{
    readonly IRecipientService _service;
        
    public ApplicationController(IRecipientService service)
    {
        _service = service;
    }
         [HttpGet]
         [Route("list/{id}")]
         public IActionResult Get(string id){
            return Ok(_service.GetListByID(id));
         }
        [HttpPost]
        public IActionResult Post([FromBody] RecipientView app)
        {
             return Ok(_service.Add(app));

        }
        [HttpPost]
		[Route("Sign/Document")]
        public IActionResult SignDocument([FromBody] RecipientView app){
            return Ok(_service.SignDocument(app));
        }




}
using Microsoft.AspNetCore.Mvc;
using pl_api.ViewModel;

[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    readonly IUsersService _usersService;

    public UsersController(IUsersService usersService)
    {
        _usersService = usersService;
    }
    [HttpGet]
    public IActionResult Get(string id){
        return Ok(_usersService.Get(id));
    }
    [HttpPost]
    public IActionResult Post([FromBody] UserView person)
    {
        return Ok(_usersService.Add(person));

    }
    [HttpPost]
    [Route("Login")]
    public IActionResult Login([FromBody] UserView value)
    {
        return Ok(_usersService.Login(value));
    }



}
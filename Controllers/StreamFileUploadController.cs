using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;

[Route("api/[controller]")]
[ApiController]
public class StreamFileUploadController : ControllerBase
{
    readonly IStreamFileUploadService _streamFileUploadService;
    readonly IMergeFilesService _mergeFilesService;



    public StreamFileUploadController(IStreamFileUploadService streamFileUploadService,IMergeFilesService mergeFilesService)
    {
        _streamFileUploadService = streamFileUploadService;
        _mergeFilesService = mergeFilesService;
    }

    // [HttpGet]
    // public IActionResult MergeFiles(){
    //     _mergeFilesService.MergePDFs();
    //     return Ok();
    // }

    [HttpPost]
    public async Task<IActionResult> SaveFileToPhysicalFolder()
    {
        var boundary = HeaderUtilities.RemoveQuotes(
            MediaTypeHeaderValue.Parse(Request.ContentType).Boundary
        ).Value;

        var reader = new MultipartReader(boundary, Request.Body);

        var section = await reader.ReadNextSectionAsync();

        string response = string.Empty;
        try
        {
            if (await _streamFileUploadService.UploadFile(reader, section))
            {
                response = "File Upload Successful";
            }
            else
            {
                response = "File Upload Failed";
            }
        }
        catch (Exception ex)
        {
            //Log ex
            response = "File Upload Failed";
        }
        return Ok(response);
    }
}
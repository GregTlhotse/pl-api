﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace pl_api.Migrations
{
    /// <inheritdoc />
    public partial class DepartmentColumn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Department",
                table: "Applications",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Department",
                table: "Applications");
        }
    }
}

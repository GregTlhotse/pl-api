
using Microsoft.EntityFrameworkCore;
using System;

namespace pl_api.ViewModel{
    public class UserView{
        public  string Id { get; set; }
        public  string Name { get; set; }
         public  string Surname { get; set; }
         public  string Email { get; set; }
         public  string Phonenumber { get; set; }
          public  string Role { get; set; }

         public  string Password { get; set; }
         public string PasswordResetCode { get; set; }
         public DateTime? PasswordResetTime { get; set; }

    }
}


using Microsoft.EntityFrameworkCore;
using System;

namespace pl_api.ViewModel{
    public class RecipientView{
        public  string Id { get; set; }
        public  string UserId { get; set; }
        public  string Name { get; set; }
         public  string Surname { get; set; }
         public  string Department { get; set; }
         public  string CompanyName { get; set; }
         public  string PaymentDescription { get; set; }
        public  string BankName { get; set; }

         public  string BankAccount { get; set; }

         public  string BranchCode { get; set; }
         public DateTime Dopr { get; set; }
         public DateTime Doi { get; set; }

    }
}
